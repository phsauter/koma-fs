# KomA FS
Diese Zusammenfassung gehört zur Vorlesung "Komplexe Analysis" (D-ITET) von Meike Akveld.
Wenn ihr irgendwelche Änderungsvorschläge habt (oder wenn ihr Fehler gefunden habt) könnt ihr mich entweder per Mail erreichen oder ihr könnt hier ein "Issue" eröffnen.
[phsauter@student.ethz.ch](phsauter@student.ethz.ch)
## Disclaimer
Das Layout wurden der Zusammenfassung von Theo von Arx (HS16-FS17; Analysis) entnommen.
Einige Befehle etc. wurden von der Zusammenfassung von Silvano Cortesi (Analysis) übernommen.
Der erste Teil des Inhaltes orierntiert sich an meiner Analysis Zusammenfassung (basierend auf Theo von Arx).
Der restliche Inhalt orierntiert sich grob an der Zusammenfassung von Thomas Etterlin (FS15; Komplexe Analysis).
Er wurde aber auf Basis des [Komplexe-Analysis Skriptes von Christian Blatter](https://people.math.ethz.ch/~blatter/dlp.html) und Wikipedia stark ergänzt.
**Es ist gut möglich, dass die Zusammenfassung inhaltliche Fehler aufweist.**
