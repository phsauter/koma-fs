
\usepackage[landscape, margin=1cm]{geometry}
\usepackage[utf8]{inputenc}

% colors
\usepackage[dvipsnames]{xcolor}

% multiple columns
\usepackage{multicol}

% multiple rows
\usepackage{multirow}

% provides fixed width tables and columns
\usepackage{tabularx}

% for \includegraphics
\usepackage{graphics}

% for wrapfigure environment
\usepackage{wrapfig}

% mathematical notation
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{interval}

% for \ul
\usepackage{soul}

% for \dcases
\usepackage{mathtools}

% for array environment
\usepackage{array}

% for pagestyle "fancy"
\usepackage{fancyhdr}

% generates the table of contents for pdf viewers
\usepackage{hyperref}
\hypersetup{pdftex,colorlinks=true,allcolors=blue}
\usepackage{hypcap}

% for nice multi-line framed boxes
\usepackage{varwidth}

% for arrow and dots in \xvec (the \vec replacement)
\usepackage{tikz}

% make document compact
%\usepackage[moderate]{savetrees}
\usepackage[compact]{titlesec}
\titlespacing{\section}{0pt}{*0}{*0}
\titlespacing{\subsection}{0pt}{*0}{*0}
\titlespacing{\subsubsection}{0pt}{*0}{*0}

%disable indentation of the first line of each paragraph
\setlength{\parindent}{0pt}

\pagestyle{fancy}
% unitlength = 1cm;
\setlength{\unitlength}{1cm}

\setcounter{secnumdepth}{0} %no enumeration of sections
\setcounter{tocdepth}{3}
% --------------------------------------------------------


\def \imgPath {img/} % relative to main file not the header

% custom sections (color boxed)

% define some colors
\definecolor{title}{HTML}{A3A3C2}
\definecolor{subtitle}{HTML}{C2C2D6}
\definecolor{text}{RGB}{0,0,0}
\definecolor{formulabox}{RGB}{255,130,0}

% highlight colors
\definecolor{hlyellow}{HTML}{EEFF40}	% yellow
\definecolor{hlgreen}{HTML}{8FFF40}		% green
\definecolor{hlorange}{HTML}{FFC300}	% orange
\definecolor{hlmagenta}{HTML}{FF408F}	% magenta


%section color box
\setkomafont{section}{\mysection}
\newcommand{\mysection}[1]{%
	\vspace*{5pt}
	\Large\sffamily\bfseries%
	\setlength{\fboxsep}{0cm} % already boxed
	\colorbox{title}{%
		\hspace{1pt}
		\begin{minipage}{\linewidth-1pt}%
			\vspace*{1pt}% Space before
			#1
			\vspace*{0pt}% Space after
		\end{minipage}%
	\vspace*{1pt}
}}
%subsection color box
\setkomafont{subsection}{\mysubsection}
\newcommand{\mysubsection}[1]{%
	\vspace*{1pt}
	\normalsize \sffamily\bfseries%
	\setlength{\fboxsep}{0cm} % already boxed
	\colorbox{subtitle}{%
		\hspace{1pt}
		\begin{minipage}{\linewidth}%
			\vspace*{1pt} %Space before
			#1
			\vspace*{0pt} %Space after
		\end{minipage}%
	\vspace*{1pt}
}}

%subsubsection color box
\setkomafont{subsubsection}{\mysubsubsection}
\newcommand{\mysubsubsection}[1]{%
	\vspace*{1pt}
	\normalsize \sffamily%
	\setlength{\fboxsep}{0cm}%already boxed
	\colorbox{subtitle!50}{%
		\hspace{1pt}
		\begin{minipage}{\linewidth}%
			\vspace*{1pt} % Space before
			#1
			\vspace*{0pt} % Space after
		\end{minipage}%
	\vspace*{1pt}
}}
% --------------------------------------------------------

% centers a certain part
\newcommand{\cenv}[1]{
	\begin{center}
		#1
	\end{center}
}

% framed and centered part of text
\newcommand{\fcenv}[1]{
	\begin{center}
	\fbox{
		\begin{varwidth}{\textwidth}
			\centering
			#1
		\end{varwidth}}
	\end{center}
}

% equation box -> use cenv{} instead so the $$ env. shows up in the code
\newcommand{\eqbox}[1]{
	\begin{center}
		$\displaystyle #1$
	\end{center}
}

% framed equation box -> use fcenv{} instead so the $$ env. shows up in the code
\newcommand{\feqbox}[1]{
	\begin{center}
		\fcolorbox{black}{white}{$\displaystyle #1$}
	\end{center}
}

% compact itemize (bullet-list)
\newenvironment{citemize}{
	\begin{itemize}
		\setlength{\itemsep}{0pt}
		\setlength{\topsep}{0pt} 
		\setlength{\partopsep}{0pt}
		\setlength{\parsep}{0pt}
	\setlength{\parskip}{0pt}}
{\end{itemize}}

% compact enumerate (numbered list)
\newenvironment{cenumerate}{
	\begin{enumerate}
		\setlength{\itemsep}{0pt}
		\setlength{\topsep}{0pt} 
		\setlength{\partopsep}{0pt}
		\setlength{\parsep}{0pt}
	\setlength{\parskip}{0pt}}
{\end{enumerate}}


%\newcommand{\comment}[1]{} % use \comment{code} to comment the code out

% highlight in math mode (highlight things inside an equation)
\newcommand{\hltm}[2]{\colorbox{#1}{$\displaystyle #2$}}
\newcommand{\hltmy}[1]{\colorbox{hlyellow}{$\displaystyle #1$}}

% highlight in text mode
\newcommand{\hlt}[2]{\colorbox{#1}{#2}}
\newcommand{\hlty}[1]{\colorbox{hlyellow}{#1}}

% set width scale factor for graphics
\newcommand{\imgfactor}{0.5}

% Shorthands for brackets and stuff
\newcommand{\brackets}[1]{\left( {#1} \right)}
\newcommand{\bslash}{\char`\\}

% Shorthands for differentials
\newcommand{\dx}{\; dx}
\newcommand{\ddx}{\; \frac{d}{dx}}
\newcommand{\dt}{\; dt}
\newcommand{\ddt}{\; \frac{d}{dt}}
\newcommand{\dz}{\; dz}
\newcommand{\ddz}{\; \frac{d}{dz}}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\newcommand*\rfrac[2]{{}^{#1}\!/_{#2}} % writes fractions like this x/y

% Shorthands for Limes
\DeclareMathOperator{\limni}{\lim\limits_{n\to\infty}}
\DeclareMathOperator{\limxi}{\lim\limits_{x\to\infty}}
\DeclareMathOperator{\limxo}{\lim\limits_{x\to0}}
\newcommand{\limx}[1]{\ensuremath{\lim\limits_{x\to #1}}}

% Math Sets
\DeclareMathOperator{\real}{\mathbb{R}}
\let\natural\relax% Undefine \natural operator so it can be set to a new value (use \sharp instead)
\DeclareMathOperator{\natural}{\mathbb{N}}
\DeclareMathOperator{\complex}{\mathbb{C}}
\DeclareMathOperator{\integer}{\mathbb{Z}}

% Math Operators
\newcommand{\abs}[1]{\ensuremath{\left\vert#1\right\vert}}
\newcommand{\norm}[1]{\left\lVert#1\right\rVert}

% Functions
\DeclareMathOperator{\arccot}{arccot}
\DeclareMathOperator{\arsinh}{arsinh}
\DeclareMathOperator{\arcosh}{arcosh}
\DeclareMathOperator{\artanh}{artanh}
\DeclareMathOperator{\arcoth}{arcoth}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\Log}{Log}
\DeclareMathOperator{\Arg}{Arg}

% Sets Operators
\newcommand{\dom}[1]{\textbf{dom}(#1)}
\newcommand{\range}[1]{\textbf{range}(#1)}
\newcommand{\image}[1]{\textbf{image}(#1)}

% Complex Operators
\DeclareMathOperator{\ImPart}{\textbf{Im}}
\DeclareMathOperator{\RePart}{\textbf{Re}}
\newcommand{\conj}[1]{\overline{#1}} % conjugate
\DeclareMathOperator{\res}{\text{res}} % residue
\DeclareMathOperator{\ind}{\text{ind}} % winding number

% Vektor Operators
\newcommand{\dotprod}{{\, \scriptscriptstyle \stackrel{\circ} {{}} \,}}
% reasonably sized circle for scalar product
\DeclareMathOperator{\laplace}{\Delta} % Laplace operator
\let\div\relax% Undefine \div operator so it can be set to a new value
\DeclareMathOperator{\div}{\textbf{div}}
\DeclareMathOperator{\rot}{\textbf{rot}}
\DeclareMathOperator{\grad}{\textbf{grad}}


% makes \widecheck{} a thing (upside down \widehat) for inverse fourier
\makeatletter
\DeclareRobustCommand\widecheck[1]{{\mathpalette\@widecheck{#1}}}
\def\@widecheck#1#2{%
	\setbox\z@\hbox{\m@th$#1#2$}%
	\setbox\tw@\hbox{\m@th$#1%
		\widehat{%
			\vrule\@width\z@\@height\ht\z@
			\vrule\@height\z@\@width\wd\z@}$}%
	\dp\tw@-\ht\z@
	\@tempdima\ht\z@ \advance\@tempdima2\ht\tw@ \divide\@tempdima\thr@@
	\setbox\tw@\hbox{%
		\raise\@tempdima\hbox{\scalebox{1}[-1]{\lower\@tempdima\box
				\tw@}}}%
	{\ooalign{\box\tw@ \cr \box\z@}}}
\makeatother


% --- Macro \xvec which provides auto-sized vectors
\makeatletter
\newlength\xvec@height%
\newlength\xvec@depth%
\newlength\xvec@width%
\newcommand{\xvec}[2][]{%
	\ifmmode%
	\settoheight{\xvec@height}{$#2$}%
	\settodepth{\xvec@depth}{$#2$}%
	\settowidth{\xvec@width}{$#2$}%
	\else%
	\settoheight{\xvec@height}{#2}%
	\settodepth{\xvec@depth}{#2}%
	\settowidth{\xvec@width}{#2}%
	\fi%
	\def\xvec@arg{#1}%
	\def\xvec@dd{:}%
	\def\xvec@d{.}%
	\raisebox{.2ex}{\raisebox{\xvec@height}{\rlap{%
				\kern.05em%  (Because left edge of drawing is at .05em)
				\begin{tikzpicture}[scale=1]
				\pgfsetroundcap
				\draw (.05em,0)--(\xvec@width-.05em,0);
				\draw (\xvec@width-.05em,0)--(\xvec@width-.15em, .075em);
				\draw (\xvec@width-.05em,0)--(\xvec@width-.15em,-.075em);
				\ifx\xvec@arg\xvec@d%
				\fill(\xvec@width*.45,.5ex) circle (.5pt);%
				\else\ifx\xvec@arg\xvec@dd%
				\fill(\xvec@width*.30,.5ex) circle (.5pt);%
				\fill(\xvec@width*.65,.5ex) circle (.5pt);%
				\fi\fi%
				\end{tikzpicture}%
	}}}%
	#2%
}
\makeatother

% --- Override \vec with an invocation of \xvec.
\let\stdvec\vec
\renewcommand{\vec}[1]{\xvec[]{#1}}
% --- Define \dvec and \ddvec for dotted and double-dotted vectors.
\newcommand{\dvec}[1]{\xvec[.]{#1}}
\newcommand{\ddvec}[1]{\xvec[:]{#1}}


% do not stretch stuff over the whole page
\raggedbottom

% Compact equations
\allowdisplaybreaks
