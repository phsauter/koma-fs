\section{Komplexe Integrale}
	\subsection{Definition}
		Ist $f: [a;b] \to \complex$ eine komplexwertige Funktion, so ist das Integral definiert als:
		\cenv{$\displaystyle\int\limits_{a}^b f(x) \dx = \displaystyle\int\limits_{a}^b Re(f(x)) \dx + i \cdot \displaystyle\int\limits_{a}^b Im(f(x)) \dx$}
		
		
	\subsection{Kurvenintegrale}
		Das komplexe Kurvenintegral ist analog zum Kurvenintegral über ein Vektorfeld der Analysis.\\
		$f:\complex \to \complex$, Paramtetrisierung der Kurve $\gamma: [a;b] \to \complex$ dann gilt:
		\cenv{$\displaystyle\int\limits_{\gamma} f(z)\dz = \displaystyle\int\limits_{a}^b f(\gamma(t))\cdot \dot{\gamma}(t)\dt$ \quad wobei $\dt$ wieder reell ist}
		\cenv{\includegraphics[width=0.7\columnwidth,keepaspectratio=true]{\imgPath vektorfeld_kurvenintegral.png}}
		Man sieht wie das Kurvenintegral den tangentialen Anteil des Vektorfeldes über die Kurve integriert.
		
		\subsubsection{Eigenschaften}
		\begin{cenumerate}
			\item Linearität (wie im reellen)
			\item $\int\limits_{-\gamma} f(z) \dz = -\int\limits_{\gamma} f(z) \dz$ wobei $-\gamma$ die in umgekehrter Richtung durchlaufene Kurve $\gamma$ ist.
			\item Ist $\abs{f(z)}\leq C$ in allen Punkten $z \in \gamma$, so gilt: $\abs{\int\limits_{\gamma}f(z)\dz} \leq L(\gamma)C$ wobei $L(\gamma)$ die Länge von $\gamma$ ist
			\item $\gamma$ kann auch eine stückweis stetige Kurve $\gamma = \sum\limits_{i=1}^n \gamma_i$ sein.
			\item unterschiedliche Wege zwischen $a$ und $b$ können zu unterschiedlichen Integralen führen.
		\end{cenumerate}
		
		\subsubsection{Parametrisierung}
			Der Weg $\gamma$ kann grundsätzlich beliebig paramterisiert werden aber einige Parametrisierungen bieten sich besonders an\\
			\begin{citemize}
				\item gerader Weg von a nach b: $\gamma (t) = a(1-t) + bt = a + t(b-a) \quad 0\leq t<1; \dot{\gamma}(t) = b-a$
				\item Kreis im Gegenuhrzeigersinn mit Radius $r$ um $a$: $\gamma (t) = a + r\cdot e^{i\phi} \quad 0\leq \phi<2\pi; \dot{\gamma}(\phi) = ir\cdot e^{i\phi}$
				\item Kreis im Uhrzeigersinn: $\gamma (t) = a + r\cdot e^{-i\phi} \quad 0\leq \phi<2\pi; \dot{\gamma}(\phi) = -ir\cdot e^{-i\phi}$
			\end{citemize}
		
		\subsection{Berechnungswege}
			Das Kurvenintegral kann man je nach Siuation verschieden berechnen.
			\begin{tabular}{p{2cm}p{6.4cm}}
				Parametrisierung (direkt) & immer\\
				Stammfunktion & $f$ holomorph, Gebiet einfach zusammenhängend\\
				Residuensatz & $f$ geschlossene Kurve, holomorph bis auf isolierte Singularitäten\\
				Satz von Cauchy & geschlossene Kurve, $f$ im inneren der Kurve holomorph\\
				Integralformel von Cauchy & geschlossene Kurve, $f$ hat nur ein Pol $n$-ter Ordnung
			\end{tabular}
		
		\subsubsection{Satz der Stammfunktionen}
			Sei $U \subseteq \complex$ ein offen zusammenhängendes Gebiet und $f:U \to \complex$ stetig dann sind folgende Aussagen äquivalent:
			\begin{cenumerate}
				\item Für jede geschlossene Kurve gilt: $\int\limits_{\gamma} f(z) \dz = 0$
				\item $\exists$ eine $\complex$-diff.bare Funktion $F:U \to \complex$ mit $\ddz F(z) = f(z)$
			\end{cenumerate}
		
	
	\subsection{Integralsatz von Cauchy}
		Sei $U \subseteq \complex$ ein einfach zusammenhängendes Gebiet und $f:U \to \complex$ holomorph.
		Dann gilt für jede geschlossene Kurve $\gamma$ (Zyklus):
		\cenv{$\displaystyle\oint\limits_{\gamma} f(z) \dz = 0$}
		Somit gilt auch für alle Kurven $\gamma_1$ und $\gamma_2$ mit demselben Anfangs und Endpunkt:
		\cenv{$\displaystyle\int\limits_{\gamma_1} f(z) \dz = \displaystyle\int\limits_{\gamma_2} f(z) \dz$}
	
	
	\subsection{Integralformel von Cauchy}
		Sei $U \subseteq \complex$ ein einfach zusammenhängendes Gebiet, $f:U \to \complex$ holomorph und $\gamma$ ein Zyklus welcher den Punkt $z_0$ einmal im Uhrzeigersinn umläuft, dann gilt:
		\cenv{$f(z_0) = \frac{1}{2\pi i}\displaystyle\int\limits_{\gamma} \frac{f(z)}{z-z_0} \dz$}
		Für Ableitungen von $f$ gilt die folgende Formel:
		\cenv{$f^{(n)}(z_0) = \frac{n!}{2\pi i}\displaystyle\int\limits_{\gamma} \frac{f(z)}{(z-z_0)^{n+1}} \dz$}
	
	
		\subsubsection{Mittelwertsatz}
			Sei $f:U \to \complex$ holomorph auf dem beliebigen Gebiet $U$ und $B(r,a)$ eine geschlossene Kreisscheibe in $U$ dann gilt:
			\cenv{$f(a) = \frac{1}{2\pi} \int\limits_{0}^{2\pi} f(a + r\cdot e^{i\phi}) \text{d}\phi$}
			Der Wert von $f$ im Mittelpunkt einer Kreisscheibe ist somit gleich dem Mittelwert von $f$ über den Rand.
			
	\subsection{Maximumprinzip}
		Wenn $f:U \to \complex$ holomorph ist und $z_0\in U$, so dass $\abs{f(z_0)} = \min(\abs{f(z)})$ oder $\max(\abs{f(z)})$, dann ist $f(z)$ konstant in $U$.\\
		$f(z)$ nimmt keine Extremalwerte in einem holomorphen Gebiet an, ausser wenn $f(z)$ konstant ist. $\min$ und $\max$ gibt es somit nur im Rand des Gebietes.