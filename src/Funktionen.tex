\section{Funktionen}
	\subsection{Injektiv, Surjektiv und Bijektiv}
		\begin{minipage}{\columnwidth} % to prevent column break between explanation and pictures
		Eine Funktion $f: X \rightarrow Y$ ist\\
		\begin{tabular}{ll}
			injektiv & falls $\forall x_1,x_2 \in X \text{ gilt } f(x_1) = f(x_2) \Rightarrow x_1=x_2$\\
			surjektiv & falls $\forall y \in Y~ \exists x \in X : f(x) = y$\\
			bijektiv & falls $f$ injektiv und surjektiv ist
		\end{tabular}\\
		\begin{tabular}{>{\centering\arraybackslash}p{0.3\columnwidth} % all columns centered with fixed width
						>{\centering\arraybackslash}p{0.3\columnwidth}
						>{\centering\arraybackslash}p{0.3\columnwidth}}
			\includegraphics[width=\linewidth,keepaspectratio=true]{\imgPath injektiv.png} &
			\includegraphics[width=\linewidth,keepaspectratio=true]{\imgPath surjektiv.png} &
			\includegraphics[width=\linewidth,keepaspectratio=true]{\imgPath bijektiv.png}\\
			\emph{injektiv} & \emph{surjektiv} & \emph{bijektiv} 
		\end{tabular}\\
		\end{minipage}
	
	\subsection{Umkehrfunktion}
		Jede injektive Funktion $f: X\rightarrow Y$ hat eine \textbf{Umkehrfunktion}
		\cenv{$f^{-1}: \image{f}\rightarrow \dom{f}, \qquad f^{-1}(y) := x\in \dom{f} : f(x) = y$}
	
		
	\subsection{Monotonie}
	Eine Funktion $f: X \rightarrow Y$ ist\\
		\begin{tabular}{ll}
			monoton steigend & falls $\forall x > x_0 : f(x) \geq f(x_0)$  bzw. $f'(x) \geq 0$\\
			streng monoton steigend & falls $\forall x > x_0 : f(x) > f(x_0)$  bzw. $f'(x) > 0$\\
			monoton fallend & falls $\forall x < x_0 : f(x) \leq f(x_0)$  bzw. $f'(x) \leq 0$\\
			streng monoton steigend & falls $\forall x > x_0 : f(x) < f(x_0)$  bzw. $f'(x) < 0$\\
			konstant & falls $\forall x \neq x_0 : f(x) = f(x_0)$  bzw. $f'(x) = 0$
		\end{tabular}
	
	
	\subsection{Gerade / Ungerade}
	Eine Funktion $\real \rightarrow \real$ ist\\
	\begin{tabular}{ll}
		gerade & falls $\forall x \in \real : f(-x) = f(x)$ (Spiegelsym. bez. y-Achse) \\
		ungerade & falls $\forall x \in \real : f(-x) = -f(x)$ (Punktsym. bez. Ursprung)
	\end{tabular}\\
	Jede Funktion lässt sich als Summe einer geraden und einer ungeraden Funktion darstellen.\\
	\cenv{$f(x) = \frac{f(x) + f(-x)}{2} + \frac{f(x) \hltmy{-} f(-x)}{2}$}
	wobei $\rfrac{1}{2}(f(x) + f(-x))$ gerade ist und $\rfrac{1}{2}(f(x) \hltmy{-} f(-x))$ ungerade.
	

	\subsection{Wichtige Funktionen}
		\subsubsection{Die Exponentialfunktion}
			\fcenv{$\exp z := \sum_{k=0}^\infty \frac{z^k}{k!} \qquad \forall z \in \complex$}
			$e := \exp 1 = \sum_{k=0}^\infty \frac{1}{k!}$ \\
			$e^z = \exp z \qquad \forall z \in \complex$ \\
			$\exp(z+w) = \exp z + \exp w \qquad \forall z, w \in \complex$ \\
			Die Exponentialfunktion ist auf $\real$ positiv und streng monoton wachsend.
			Sie wächst für $x\rightarrow\infty$  schneller als jede feste Potenz von $x$.
			Mithilfe des Logarithmus lässt sich die \textbf{allgemeine Potenz} definieren:
			\fcenv{$a^x := e^{x \cdot \log a} \qquad a > 0$}
	
		\subsubsection{Die Logarithmusfunktion}
			Der \textbf{natürliche Logarithmus} ist die Umkehrfunktion der Exponentialfunktion:
			$\log: \real_{>0} \rightarrow \real \qquad \log = \exp^{-1}$\\
			Für alle Logarithmen gelten folgene Rechenregeln:
			\begin{citemize}
				\item		$\log x + \log y = \log(x \cdot y)$
				\item		$\log x^{\frac{p}{q}} = \frac{p}{q} \log x$
				\item		$\log_b a = \frac{\log_c a}{\log_c b} = \frac{\log a}{\log b}$
			\end{citemize}
			Der Logarithmus geht auf $\real_{>0}$ von $-\infty$ bis $\infty$ und ist streng monoton wachsend.
			Er wächst für $x\rightarrow\infty$  langsamer als jede feste Potenz von $x$.				
		
		\subsubsection{Trigonometrische Funktionen}
			\fcenv{$\sin (t) = \frac{e^{i \cdot t} - e^{-i \cdot t}}{2 \cdot i}$}
			\fcenv{$\cos (t) = \frac{e^{i \cdot t} + e^{-i \cdot t}}{2}$}				
		
		\subsubsection{Hyperbolische Funktionen}
			Die hyperbolischen Funktionen $\cosh$ und $\sinh$ entstehen durch Aufteilen der Exponentialfunktion
			in einen geraden Anteil ($\cosh$) und einen ungeraden Anteil ($\sinh$).
			
			\cenv{$\cosh t := \frac{e^t + e^{-t}}{2}, \qquad \arcosh t := \log(t + \sqrt{t^2 - 1}), ~x \geqslant 1$}
			\cenv{$\sinh t := \frac{e^t - e^{-t}}{2}, \qquad \arsinh t := \log(t + \sqrt{t^2 + 1})$}
			\cenv{$\tanh t := \frac{\sinh t}{\cosh t} = \frac{e^t - e^{-t}}{e^t + e^{-t}}$}
